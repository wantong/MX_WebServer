package webserver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.net.Socket;

public class Processor extends Thread {
	private InputStream stream_in;
	private PrintStream stream_out;
	private final static String WEBROOT="c://web_root";
	public Processor(Socket s) {
		try {
			this.stream_in=s.getInputStream();
			this.stream_out=new PrintStream(s.getOutputStream());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void run(){
		String filename=parseFilename();
		sendFile(filename);
	}
	
	private String parseFilename(){
		String filename=null;
		BufferedReader br=new BufferedReader(new InputStreamReader(stream_in));
		try {
			String httpMsg=br.readLine();
			System.out.print(httpMsg);
			//浏览器有时会发送一些空的请求，直接无视
			if(httpMsg==null){
				System.out.print("--->无视");
				System.out.println();
				return null;
			}
			String[] msgs=httpMsg.split(" ");
			if(msgs.length!=3){
				sendErrorMsg(400,"Bad Request","客户端请求的语法错误，服务器无法理解");
				return null;
			}
			filename=msgs[1];
		} catch (IOException e) {
			e.printStackTrace();
		}
		return filename;
	}
	
	private void sendFile(String filename){
		if(filename==null){
			return;
		}
		File file=new File(Processor.WEBROOT+filename);
		try {
			if(!file.exists()){
				sendErrorMsg(404,"Not Found","服务器无法根据客户端的请求找到资源（网页）");
				return;
			}
			InputStream in=new FileInputStream(file);
			byte[] content=new byte[(int)file.length()];
			in.read(content);
			stream_out.println("HTTP/1.1 200 OK");
			stream_out.println("Content-Length: "+content.length);
			stream_out.println();
			stream_out.write(content);
			stream_out.flush();
			stream_out.close();
			stream_in.close();
			System.out.print("--->成功响应");
			System.out.println();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
		
	private void sendErrorMsg(int errCode, String errMsg, String errExplanation) {
		try {
			//使用OutputStreamWriter来包裹PrintStream，输出中文错误提示
			OutputStreamWriter osw=new OutputStreamWriter(stream_out, "GBK");
			osw.write("HTTP/1.0 "+errCode+" "+errMsg+"\n");
			osw.write("Content-Type: text/html"+"\n");
			osw.write("\n");
			osw.write("<html>"+"\n");
			osw.write("<title>"+errCode+"</title>"+"\n");
			osw.write("<body>"+"\n");
			osw.write("<h2>"+errCode+" "+errMsg+"</h2><br/>"+"\n");
			osw.write("<h2>"+errExplanation+"</h2>"+"\n");
			osw.write("</body>"+"\n");
			osw.write("</html>"+"\n");
			osw.flush();
			osw.close();
			stream_in.close();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.print("--->响应失败");
		System.out.println();
	}
}
