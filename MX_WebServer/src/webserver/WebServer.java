package webserver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class WebServer {
	
	public void Start(int port){
		try {
			ServerSocket serverSocket=new ServerSocket(port);
			System.out.println("服务器启动成功");
			while(true){
				Socket socket=serverSocket.accept();
				new Processor(socket).start();
			}
		} catch (IOException e) {
			System.out.println("服务器启动失败");
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		int port=80;
		if(args.length==1){
			port=Integer.parseInt(args[0]);
		}
		new WebServer().Start(port);
	}

}
